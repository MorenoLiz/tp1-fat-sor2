#include <stdio.h>
#include <stdlib.h>

typedef struct {
    unsigned char first_byte;
    unsigned char start_chs[3];
    unsigned char partition_type;
    unsigned char end_chs[3];
    unsigned short starting_cluster;
    unsigned int file_size;
} __attribute((packed)) PartitionTable;

typedef struct {
    unsigned char jmp[3];
    char oem[8];
    unsigned short sector_size;	
    unsigned char sectors_per_cluster; 
    unsigned short size_reserved_area; 
    unsigned char number_fats; 
	unsigned short maximum_number_files; 
	unsigned short number_sectors; 
    unsigned char media_type; 
    unsigned short size_each_fat; 
    unsigned short sector_per_track; 
    unsigned short number_heads; 
    unsigned int number_sector_before_start_partition; 
    unsigned int fs_number_sectors; 
    unsigned char drive_number; 
    unsigned char not_used; 
    unsigned char extended_boot_signature;     
    char volume_id[4];
    char volume_label[11];
    char fs_type[8];
    char boot_code[448];
    unsigned short boot_sector_signature;
} __attribute((packed)) Fat12BootSector;

typedef struct {    
    unsigned char filename[1]; 
    unsigned char filename_ascii[7];
    unsigned char file_extension[3];
    unsigned char file_attributes[1]; 
    unsigned char reserved[1];
    unsigned char created_time_seconds[1];
    unsigned short created_time_hours_min_seconds; 
    unsigned short created_day; 
    unsigned short access_day; 
    unsigned short high_2b_cluster; 
    unsigned short modified_time; 
    unsigned short modified_day; 
    unsigned short low_2b_cluster; //low-order 2 bytes of address of first cluster
    unsigned int size_file; 
    
} __attribute((packed)) Fat12Entry;

void write_recupero_file(unsigned short pos){
    FILE * in = fopen("test.img", "r+");
    char char1[1] ="M";
    fseek(in, pos , SEEK_SET);
    fwrite(char1, sizeof(char), sizeof(char1), in);
    printf("SUCCESSFUL...\n\n");
            
    fclose(in);
}

void print_file_info(Fat12Entry *entry, unsigned short pos){
    switch(entry->filename[0])
    {
        case 0x00:            
            return;// unused entry
        case 0xE5:
            printf("Archivo a recuperar: [%c%.7s.%.3s]\n", 0xE5, entry->filename_ascii, entry->file_extension);
            write_recupero_file(pos);    
            return;
        default:
            return;
    }
}

int main() {
    FILE * in = fopen("test.img", "rb");
    int i;
    PartitionTable pt[4];
    Fat12BootSector bs;
    Fat12Entry entry;
    unsigned short pos;

    fseek(in, 0x1BE , SEEK_SET);
    fread(pt, sizeof(PartitionTable), 4, in);        
    for(i=0; i<4; i++) {        
        if(pt[i].partition_type == 1){
            printf("Encontrada particion FAT12 %d\n", i);
            break;
        }
    }
    
    if(i == 4) 
    {
        printf("No encontrado filesystem FAT12, saliendo...\n");
        return -1;
    }
    
    fseek(in, 0, SEEK_SET);    
    fread(&bs, sizeof(Fat12BootSector), 1, in);

    // printf("En  0x%lX, sector size %d, FAT size %d sectors, %d FATs\n\n", 
    //        ftell(in), bs.sector_size, bs.size_each_fat, bs.number_fats);  //ftell(in):pos actual en el file
       
    fseek(in, (bs.size_reserved_area-1 + bs.size_each_fat * bs.number_fats) *
          bs.sector_size, SEEK_CUR);    
    
    for(i=0; i<bs.maximum_number_files; i++){       
        pos = ftell(in);        
        fread(&entry, sizeof(entry), 1, in);
        print_file_info(&entry, pos);
    }
    
    fclose(in);
    return 0;
}