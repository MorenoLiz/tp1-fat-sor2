#include <stdio.h>
#include <stdlib.h>
/*
borrarlo
typedef unsigned char BYTE;
 
typedef unsigned short WORD;
 
typedef unsigned long DWORD;
*/
typedef struct {
    unsigned char first_byte;
    unsigned char start_chs[3];
    unsigned char partition_type;
    unsigned char end_chs[3];    
    unsigned short starting_cluster;
    unsigned int file_size;
} __attribute((packed)) PartitionTable; //DOS Partition Entry Data Structure inside MBR 

typedef struct {
    unsigned char jmp[3];
    char oem[8];
    unsigned short sector_size; // 2 bytes
    // 

    unsigned char sector_per_cluster;
    unsigned short size_reserved_area;
    unsigned char number_fats;
    unsigned short maximum_number_files;
    unsigned short number_sectors;
    unsigned char media_type;
    unsigned short size_each_fat;
    unsigned short sector_per_track;
    unsigned short number_heads;
    unsigned int number_sector_before_start_partition;
    unsigned int fs_number_sectors;
    unsigned char drive_number;
    unsigned char not_used;
    unsigned char extended_boot_signature; 	
    //
    unsigned int volume_id;
    char volume_label[11];
    char fs_type[8]; // Type en ascii
    char boot_code[448];
    unsigned short boot_sector_signature;
} __attribute((packed)) Fat12BootSector;

int main() {
    FILE * in = fopen("test.img", "rb");
    int i;
    PartitionTable pt[4];
    Fat12BootSector bs;
    
    fseek(in, 446, SEEK_SET); // Ir al inicio de la tabla de particiones
    fread(pt, sizeof(PartitionTable), 4, in); // leo entradas 
    
    for(i=0; i<4; i++) {        
        printf("Partition type: %d\n", pt[i].partition_type);
        if(pt[i].partition_type == 1) {
            printf("Encontrado FAT12 %d\n", i);
            break;
        }
    }
    
    if(i == 4) {
        printf("No se encontró filesystem FAT12, saliendo ...\n");
        return -1;
    }
    
    fseek(in, 0, SEEK_SET);
    fread(&bs, sizeof(Fat12BootSector), 1, in);
    
    printf("  Jump code: %02X:%02X:%02X\n", bs.jmp[0], bs.jmp[1], bs.jmp[2]); //CHAR
    printf("  OEM code: [%.8s]\n", bs.oem);
    printf("  Bytes per sector: %d\n", bs.sector_size);//SHORT

    printf("  Sectors per cluster: %hhu\n", bs.sector_per_cluster);
    printf("  Size of reserved area: %u\n", bs.size_reserved_area);
    printf("  Number of FATs: %hhu\n", bs.number_fats);
    printf("  Maximum number of files in the root directory: %u\n",bs.maximum_number_files);
    printf("  Number of sectors in the FS: %u\n",bs.number_sectors);
    printf("  Media type: %hhu\n",bs.media_type);
    printf("  Size of each FAT: %u\n",bs.size_each_fat);
    printf("  Sectors per track: %u\n",bs.sector_per_track);
    printf("  Number of heads: %u\n",bs.number_heads);
    printf("  Number of sectors before start partition: %d.\n",bs.number_sector_before_start_partition);
    printf("  Number of sectors the FS: %d.\n",bs.fs_number_sectors);
    printf("  Drive number: %hhu\n",bs.drive_number);
    printf("  Not used: %hhu\n",bs.not_used);
    printf("  Extend boot signature: %hhu\n",bs.extended_boot_signature);
	
    printf("  volume_id: 0x%08X\n", bs.volume_id);
    printf("  Volume label: [%.11s]\n", bs.volume_label);
    printf("  Filesystem type: [%.8s]\n", bs.fs_type);
    printf("  Boot sector signature: 0x%04X\n", bs.boot_sector_signature);
    
    fclose(in);
    return 0;
}
