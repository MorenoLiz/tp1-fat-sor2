#include <stdio.h>
#include <stdlib.h>

typedef struct {
    unsigned char first_byte;
    unsigned char start_chs[3];
    unsigned char partition_type;
    unsigned char end_chs[3];
    unsigned short starting_cluster;
    unsigned int file_size;
} __attribute((packed)) PartitionTable;

typedef struct {
    unsigned char jmp[3];
    char oem[8];
    unsigned short sector_size;	
    unsigned char sector_per_cluster;
    unsigned short size_reserved_area;
    unsigned char number_fats;
    unsigned short maximum_number_files;
    unsigned short number_sectors;
    unsigned char media_type;
    unsigned short size_each_fat;
    unsigned short sector_per_track;
    unsigned short number_heads;
    unsigned int number_sector_before_start_partition;
    unsigned int fs_number_sectors;
    unsigned char drive_number;
    unsigned char not_used;
    unsigned char extended_boot_signature;	
    unsigned int volume_id;
    char volume_label[11];
    char fs_type[8];
    char boot_code[448];
    unsigned short boot_sector_signature;
} __attribute((packed)) Fat12BootSector;

typedef struct {	
    char filename[1];
    char filename_ascii[7]; 
    char file_extension [3];
    char file_attributes[1];
    unsigned char reserved;
    unsigned char created_time_seconds;
    unsigned char created_time_hours_min_seconds[2];
    unsigned char created_day[2];
    unsigned char access_day[2];
    unsigned char high_2b_cluster[2];
    unsigned char modified_time[2];
    unsigned char modified_day[2];
    unsigned short low_2b_cluster; //low-order 2 bytes of address of first cluster
    unsigned int size_file;
    
} __attribute((packed)) Fat12Entry;
void read_file_info(unsigned short low_2b_cluster, int size_file, unsigned short first_cluster, unsigned short size_cluster) {
    FILE * in = fopen("test.img", "rb");    
    char read1[size_file];    
    fseek(in, first_cluster + ((low_2b_cluster - 2) * size_cluster) , SEEK_SET);
    fread(read1, size_file, 1, in);
    
    for(int i=0; i<size_file; i++){
        printf("%c", read1[i]);
    }    
    fclose(in);
}

void print_file_info(Fat12Entry *entry, unsigned short first_cluster, unsigned short size_cluster) {
    switch(entry->filename[0]) {
    case 0x00:
        return; // unused entry
    case 0xE5: //         
        printf("Archivo borrado: [?%.7s.%.3s]\n",entry->filename_ascii, entry->file_extension); // 
        read_file_info(entry->low_2b_cluster, entry->size_file, first_cluster, size_cluster);
        return;
    case 0x05 : //         
        printf("Archivo que comienza con 0xE5: [%c%.7s.%.3s]\n", 0xE5,entry->filename_ascii, entry->file_extension); 
        read_file_info(entry->low_2b_cluster, entry->size_file, first_cluster, size_cluster);
        break;
    // case 0x2E:
    //     printf("Sub directorio: [%.1s%.7s.%.3s]\n",entry->filename, entry->filename_ascii,entry->file_extension);// 
    //     read_file_info(entry->low_2b_cluster, entry->size_file, first_cluster, size_cluster);
    //     break;
    default:
        switch(entry->file_attributes[0]) {
        case 0x10 : // Completar los ...
            printf("Directorio: [%.1s%.7s.%.3s]\n",entry->filename, entry->filename_ascii,entry->file_extension);// 
            return;
        case 0x0F:
        //long file name  
            break;
        case 0x20:            
            printf("\nArchivo: [%.1s%.7s.%.3s]\n",entry->filename, entry->filename_ascii,entry->file_extension);// 
            read_file_info(entry->low_2b_cluster, entry->size_file, first_cluster, size_cluster);
            return;
        default:
            return;
        
        }   
    }
    
}

int main() {
    FILE * in = fopen("test.img", "rb");
    int i;
    PartitionTable pt[4];
    Fat12BootSector bs;
    Fat12Entry entry;
    unsigned short first_cluster;
    unsigned short size_cluster;
    	   
	
    fseek(in, 0x1BE , SEEK_SET); // Voy al inicio de la tabla de particiones BootCode
    fread(pt,sizeof(PartitionTable),4,in); //leo partition table
    for(i=0; i<4; i++) {        
        if(pt[i].partition_type == 1) {
            printf("Encontrada particion FAT12 %d\n", i);
            break;
        }
    }
    
    if(i == 4) {
        printf("No encontrado filesystem FAT12, saliendo...\n");
        return -1;
    }
    
    fseek(in, 0, SEEK_SET);
	fread(&bs,sizeof(Fat12BootSector),1,in);//{...} Leo boot sector
    
    printf("En  0x%lX, sector size %d, FAT size %d sectors, %d FATs\n\n", 
           ftell(in), bs.sector_size, bs.size_each_fat, bs.number_fats);  //ftell(in):pos actual en el file
           
    fseek(in, (bs.size_reserved_area-1 + bs.size_each_fat * bs.number_fats) *
          bs.sector_size, SEEK_CUR);

     //getAddress first_cluster
    first_cluster = ftell(in) + (bs.maximum_number_files * sizeof(entry));
    //calculo tam del cluster.
    size_cluster = bs.sector_size * bs.sector_per_cluster;      
    
    printf("Root dir_entries %d \n", bs.maximum_number_files);
    for(i=0; i<bs.maximum_number_files; i++) {
        fread(&entry, sizeof(entry), 1, in);
        print_file_info(&entry,first_cluster,size_cluster);
    }
    
    printf("\nLeido Root directory, ahora en 0x%lX\n", ftell(in)); //Pos final 2560+(512*32)=18944=0x4A00.


    //pos tercer sector
    fseek(in, (bs.size_reserved_area-1 + bs.size_each_fat * bs.number_fats) *
          bs.sector_size, SEEK_CUR);  
    printf("Root dir_entries %d \n", bs.maximum_number_files);

    //maximun_numer_files 512... print type & name of files
    for(i=0; i<bs.maximum_number_files; i++) {
        fread(&entry, sizeof(entry), 1, in);
        print_file_info(&entry,first_cluster,size_cluster);
    }
    printf("\nLeido Root directory, ahora en 0x%lX\n", ftell(in));

    fclose(in);
    return 0;
}
